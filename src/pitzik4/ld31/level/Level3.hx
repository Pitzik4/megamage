package pitzik4.ld31.level;

import com.haxepunk.Entity;
import com.haxepunk.masks.Grid;
import com.haxepunk.graphics.Tilemap;
import com.haxepunk.Graphic;
import pitzik4.ld31.*;

class Level3 extends Level {
  public function new(grid:Grid, map:Tilemap, owner:MainScene) {
    super("level3", grid, map, owner);
  }

  override public function added():Void {
    super.added();
    owner.artifact.x = 17*16;
    owner.artifact.y = 1*16;
    add(Level.makeArtifact(owner.artifact.x, owner.artifact.y));
    var hunter = new Hunter(owner, 3*16, 0*16);
    hunter.flipped = false;
    hunter.maxVelocity.x *= 0.4;
    add(hunter);
    owner.addActor("Hunter", hunter);
    owner.player.y = 16*12;
  }

  override public function reset():Level3 {
    return new Level3(grid, map, owner);
  }
  override public function getNext():LevelHome {
    return new LevelHome(grid, map, owner, LAST);
  }
}
