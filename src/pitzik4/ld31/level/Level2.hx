package pitzik4.ld31.level;

import com.haxepunk.Entity;
import com.haxepunk.masks.Grid;
import com.haxepunk.graphics.Tilemap;
import com.haxepunk.Graphic;
import pitzik4.ld31.*;

class Level2 extends Level {
  public var dialogue:Bool;

  public function new(grid:Grid, map:Tilemap, owner:MainScene, dialogue:Bool = true) {
    super("level2", grid, map, owner);
    this.dialogue = dialogue;
  }

  override public function added():Void {
    super.added();
    owner.artifact.x = 18*16;
    owner.artifact.y = 5*16;
    add(Level.makeArtifact(owner.artifact.x, owner.artifact.y));
    var hunter = new Hunter(owner, -1*16, 2*16);
    hunter.flipped = false;
    hunter.maxVelocity.x *= 0.4;
    add(hunter);
    owner.addActor("Hunter", hunter);

    if(dialogue) {
      owner.schedule(160, function():Void {
        owner.say("Game", "...");
        });
      owner.schedule(240, function():Void {
        owner.say("Game", "Is it just me, or is Mr. Robot moving at, like...");
        });
      owner.schedule(360, function():Void {
        owner.say("Game", "40% his normal speed?");
        });
      owner.schedule(510, function():Void {
        owner.say("Game", "You know what, it works in my favor. I'm not gonna question it.");
        });
    }
  }

  override public function reset():Level2 {
    return new Level2(grid, map, owner, false);
  }
  override public function getNext():Level3 {
    return new Level3(grid, map, owner);
  }
}
