package pitzik4.ld31.level;

import com.haxepunk.Entity;
import com.haxepunk.masks.Grid;
import com.haxepunk.graphics.Tilemap;
import com.haxepunk.Graphic;
import pitzik4.ld31.*;

class Level1 extends Level {
  public function new(grid:Grid, map:Tilemap, owner:MainScene) {
    super("level1", grid, map, owner);
  }

  override public function added():Void {
    super.added();
    owner.artifact.x = 17*16;
    owner.artifact.y = 10*16;
    add(Level.makeArtifact(owner.artifact.x, owner.artifact.y));
    var hunter = new Hunter(owner, 19*16, 4*16);
    hunter.flipped = true;
    add(hunter);
    owner.addActor("Hunter", hunter);
  }

  override public function reset():Level1 {
    return new Level1(grid, map, owner);
  }
  override public function getNext():Level2 {
    return new Level2(grid, map, owner);
  }
}
