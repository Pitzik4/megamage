package pitzik4.ld31.level;

import pitzik4.ld31.*;
import com.haxepunk.masks.Grid;
import com.haxepunk.graphics.Tilemap;
import com.haxepunk.graphics.Backdrop;
import com.haxepunk.graphics.Stamp;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.Entity;

enum HomeScene {
  FIRST;
  LAST;
}
class LevelHome extends Level {
  public var hunterSpr:Spritemap;
  public var hunter:Entity;
  public var which:HomeScene;
  public var end:Entity;

  public function new(grid:Grid, map:Tilemap, owner:MainScene, ?which:HomeScene) {
    super("home", grid, map, owner);
    this.which = which==null?FIRST:which;
  }
  override public function added():Void {
    super.added();
    owner.removeOtherGfc(graphic);
    addGfc(new Backdrop("graphics/bg.png"));
    addGfc(new Stamp("graphics/door.png", 3*16, 3*16));
    addGfc(new Stamp("graphics/bars.png", 14*16, 2*16));
    addGfc(new Stamp("graphics/bars.png", 5*16, 6*16));
    addGfc(new Stamp("graphics/bars.png", 2*16, 10*16));
    addGfc(new Stamp("graphics/bars.png", 10*16, 10*16));
    owner.addOtherGfc(graphic);

    switch(which) {
    case FIRST:
    owner.player.x = 11*16; owner.player.y = 3*16;
    owner.player.flipped = false;
    owner.player.action = CUTSCENE;
    owner.bgm.stop();
    var t = 0;
    owner.schedule(t+=60, function():Void {
      owner.say("Game", "(Psst. Hey, player.)");
      });
    owner.schedule(t+=120, function():Void {
      owner.say("Game", "(I'm not supposed to be talking to you, but...)");
      });
    owner.schedule(t+=180, function():Void {
      owner.say("Game", "(You can skip this cutscene by pressing Q or enter.)");
      });
    owner.schedule(t+=120, function():Void {
      hunter = new Entity(3*16, 3*16);
      hunter.graphic = hunterSpr = new Spritemap("graphics/hunter.png", 32, 32);
      hunterSpr.add("idle", [0], 1, false);
      hunterSpr.play("idle");
      hunter.setHitbox(32, 32);
      add(hunter);
      owner.addActor("Hunter", hunter);
      });
    owner.schedule(t+=60, function():Void {
      owner.say("Game", "Well, well, well. If it isn't...");
      });
    owner.schedule(t+=120, function():Void {
      owner.player.flipped = true;
      });
    owner.schedule(t+=90, function():Void {
      owner.say("Game", "Uh...");
      });
    owner.schedule(t+=60, function():Void {
      owner.say("Game", "A robot?");
      });
    owner.schedule(t+=120, function():Void {
      owner.say("Hunter", "That is correct.");
      });
    owner.schedule(t+=180, function():Void {
      owner.say("Game", "I was... sort of expecting a hero.");
      });
    owner.schedule(t+=90, function():Void {
      owner.say("Game", "You know, with a sword and stuff.");
      });
    owner.schedule(t+=120, function():Void {
      owner.say("Hunter", "Those have been obsolete for decades.");
      });
    owner.schedule(t+=120, function():Void {
      owner.say("Game", "Well, I have been a bit isolated here in this castle.");
      });
    owner.schedule(t+=120, function():Void {
      owner.say("Game", "Anyway...");
      });
    owner.schedule(t+=60, function():Void {
      owner.say("Game", "You were sent here to thwart my evil schemes, right?");
      });
    owner.schedule(t+=150, function():Void {
      owner.say("Hunter", "In a manner of speaking.");
      });
    owner.schedule(t+=120, function():Void {
      owner.player.sfxLaugh.play();
      owner.player.sprite.play("laugh");
      });
    owner.schedule(t+=120, function():Void {
      owner.say("Game", "Well, do your worst!");
      });
    owner.schedule(t+=90, function():Void {
      owner.say("Game", "No mere robot can kill one such as I!");
      });
    owner.schedule(t+=120, function():Void {
      owner.say("Game", "Or is it \"me\"? That's a bit of a grammatical edge case.");
      });
    owner.schedule(t+=150, function():Void {
      owner.say("Hunter", "I'm not here to kill you.");
      });
    owner.schedule(t+=90, function():Void {
      owner.say("Game", "You're not?");
      });
    owner.schedule(t+=60, function():Void {
      owner.say("Hunter", "No, that would be violating the First Law.");
      });
    owner.schedule(t+=150, function():Void {
      owner.say("Hunter", "I'm not allowed to kill humans.");
      });
    owner.schedule(t+=150, function():Void {
      owner.say("Game", "I take offense to your assumption that I'm human.");
      });
    owner.schedule(t+=150, function():Void {
      owner.say("Hunter", "I'm just going to seal you in your own castle.");
      });
    owner.schedule(t+=150, function():Void {
      owner.say("Hunter", "See, I collected a bunch of artifacts on the way that let me do that.");
      });
    owner.schedule(t+=180, function():Void {
      owner.say("Hunter", "I only needed one, but I took them all just to be safe.");
      });
    owner.schedule(t+=150, function():Void {
      owner.say("Game", "Sounds like robo-OCD to me.");
      });
    owner.schedule(t+=90, function():Void {
      owner.say("Game", "Anyway... I'm not going down that easily!");
      });
    owner.schedule(t+=120, function():Void {
      owner.say("Game", "I'll disintegrate you where you stand!");
      owner.player.sprite.play("cast");
      });
    owner.schedule(t+=120, function():Void {
      owner.say("Hunter", "Not if I make my escape while you pose dramatically. So long!");
      });
    owner.schedule(t+=30, function():Void {
      owner.removeActor("Hunter");
      remove(hunter);
      });
    owner.schedule(t+=150, function():Void {
      owner.say("Game", "Huh? Wait!");
      });
    owner.schedule(t+=60, function():Void {
      owner.say("Game", "You forgot to cast the spell!");
      });
    owner.schedule(t+=210, function():Void {
      owner.say("Game", "...Right?");
      });
    owner.schedule(t+=90, function():Void {
      owner.say("Game", "Am I sealed in the castle now?");
      });
    owner.schedule(t+=90, function():Void {
      owner.player.sprite.play("run");
      owner.player.acceleration.x = -Player.ACCEL_X;
      });
    owner.schedule(t+=Std.int(8*16/Player.MAX_VEL_X), function():Void {
      owner.player.sprite.play("idle");
      owner.player.acceleration.x = 0;
      });
    owner.schedule(t+=60, function():Void {
      owner.say("Game", "...");
      });
    owner.schedule(t+=120, function():Void {
      owner.say("Game", "Well, the door doesn't work.");
      });
    owner.schedule(t+=150, function():Void {
      owner.say("Game", "I'm assuming it's not just stuck and that I'm trapped here forever.");
      });
    owner.schedule(t+=240, function():Void {
      owner.say("Game", "Well... he has greatly underestimated my power!");
      });
    owner.schedule(t+=180, function():Void {
      owner.say("Game", "For I am...");
      });
    owner.schedule(t+=120, function():Void {
      owner.player.sprite.play("cast");
      owner.say("Game", "GAME THE MEGAMAGE!");
      });
    owner.schedule(t+=180, function():Void {
      owner.say("Game", "...What?");
      });
    owner.schedule(t+=120, function():Void {
      owner.say("Game", "What's so funny?");
      });
    owner.schedule(t+=120, function():Void {
      owner.say("Game", "Do you just think \"Game\" is a funny name?");
      });
    owner.schedule(t+=180, function():Void {
      owner.say("Game", "Or is there some context I'm missing?");
      });
    owner.schedule(t+=180, function():Void {
      owner.say("Game", "...Anyway!");
      });
    owner.schedule(t+=90, function():Void {
      owner.say("Game", "I will cast a spell that will send me back in time!");
      });
    owner.schedule(t+=180, function():Void {
      owner.say("Game", "And I will stop that gosh darn robot from getting even one of those artifacts!");
      });
    owner.schedule(t+=240, function():Void {
      owner.say("Game", "Here goes...");
      });
    owner.schedule(t+=180, function():Void {
      owner.player.sprite.play("cast", true);
      owner.say("Game", "BOGUS LATINUS WORDA!");
      });
    owner.schedule(t+=30, function():Void {
      owner.beginLevelSwitch(new Level1(grid, map, owner), true);
      owner.player.x = 3*16;
      });
    case LAST:
    owner.player.x = 11*16; owner.player.y = 3*16;
    owner.player.flipped = true;
    owner.player.action = CUTSCENE;
    owner.player.sprite.play("idle");
    var t = 0;
    owner.schedule(t+=60, function():Void {
      owner.say("Game", "Alright, I got 'em all.");
      });
    owner.schedule(t+=120, function():Void {
      owner.say("Game", "Now he can't use them to trap me here.");
      });
    owner.schedule(t+=270, function():Void {
      hunter = new Entity(3*16, 3*16);
      hunter.graphic = hunterSpr = new Spritemap("graphics/hunter.png", 32, 32);
      hunterSpr.add("idle", [0], 1, false);
      hunterSpr.play("idle");
      hunter.setHitbox(32, 32);
      add(hunter);
      owner.addActor("Hunter", hunter);
      });
    owner.schedule(t+=60, function():Void {
      owner.say("Game", "Hello, there, Mr. Robot.");
      });
    owner.schedule(t+=120, function():Void {
      owner.say("Game", "Didn't find any magic artifacts on the way here? How sad.");
      });
    owner.schedule(t+=180, function():Void {
      owner.say("Hunter", "That's right. How did you know that?");
      });
    owner.schedule(t+=150, function():Void {
      owner.say("Game", "When you used them to seal me in here, I went back in time.");
      });
    owner.schedule(t+=180, function():Void {
      owner.say("Game", "I stopped you from getting the artifacts...");
      });
    owner.schedule(t+=150, function():Void {
      owner.player.sprite.play("cast");
      owner.say("Game", "Thus ensuring that you couldn't trap me!");
      });
    owner.schedule(t+=150, function():Void {
      owner.say("Hunter", "So... I already used them on you in another timeline?");
      });
    owner.schedule(t+=180, function():Void {
      owner.say("Game", "Yeah.");
      });
    owner.schedule(t+=120, function():Void {
      owner.say("Hunter", "Uh. Ahem.");
      });
    owner.schedule(t+=120, function():Void {
      owner.say("Hunter", "You are aware, of course, that...");
      });
    owner.schedule(t+=180, function():Void {
      owner.say("Hunter", "Going back in time doesn't break the curse?");
      });
    owner.schedule(t+=270, function():Void {
      owner.say("Game", "What?");
      });
    owner.schedule(t+=120, function():Void {
      owner.say("Hunter", "You're still trapped.");
      });
    owner.schedule(t+=120, function():Void {
      owner.say("Hunter", "Glad that's cleared up. So long.");
      });
    owner.schedule(t+=180, function():Void {
      owner.removeActor("Hunter");
      remove(hunter);
      });
    owner.schedule(t+=270, function():Void {
      owner.player.sprite.play("cast", true);
      owner.say("Game", "NOOOOOOOOOOOOOO!");
      owner.fade(60, 0xFF000000, 0, true, false);
      });
    owner.schedule(t+=600, function():Void {
      owner.say("Game", "Well, that's all I have to say about that.");
      });
    owner.schedule(t+=600, function():Void {
      owner.say("Game", "Trapped here forever. That's me.");
      });
    owner.schedule(t+=600, function():Void {
      owner.say("Game", "Forever is a heck of a long time.");
      });
    owner.schedule(t+=600, function():Void {
      owner.say("Game", "I wonder how I'll spend it?");
      });
    owner.schedule(t+=600, function():Void {
      owner.say("Game", "Solitaire. Is there a deck of cards around here?");
      });
    owner.schedule(t+=600, function():Void {
      end = owner.addGraphic(new Stamp("graphics/the-end.png"), -1024);
      owner.fade(4, 0xFF000000, 1);
      });
    }
  }
  override public function removed():Void {
    super.removed();
    if(end != null) owner.remove(end);
    owner.bgm.play();
    var t = 0;
    var h:Hunter = null;
    owner.schedule(0, function():Void {
      h = cast owner.actors.get("Hunter");
      h.action = ANIMATE("idle");
      });
    owner.schedule(t+=90, function():Void {
      owner.player.flipped = false;
      });
    owner.schedule(t+=90, function():Void {
      owner.say("Game", "Wow, it actually worked. Sort of.");
      });
    owner.schedule(t+=150, function():Void {
      owner.say("Game", "I mean, my spell didn't work correctly?");
      });
    owner.schedule(t+=90, function():Void {
      owner.say("Game", "These artifacts must be immensely powerful!");
      });
    owner.schedule(t+=150, function():Void {
      owner.say("Game", "Well, it's fine. I have a strategy.");
      });
    owner.schedule(t+=180, function():Void {
      owner.player.action = FREE;
      owner.say("Game", "I'll use Arrow Key Magic or WASD magic to walk around.");
      });
    owner.schedule(t+=180, function():Void {
      owner.say("Game", "I'll use Z magic, Up Arrow Magic, or W magic to jump.");
      });
    owner.schedule(t+=180, function():Void {
      owner.say("Game", "I'll use X magic or Spacebar Magic to leave the spell's focal point somewhere.");
      });
    owner.schedule(t+=240, function():Void {
      owner.say("Game", "Then, I'll use X or Spacebar Magic again to move the focal point back to me.");
      });
    owner.schedule(t+=240, function():Void {
      owner.say("Game", "I'll use Q magic or Return Key Magic to reset the level if things go wrong.");
      });
    owner.schedule(t+=240, function():Void {
      owner.say("Game", "I can even use P magic to skip levels.");
      });
    owner.schedule(t+=180, function():Void {
      owner.say("Game", "I have no idea why, but I don't really feel like using P magic very much.");
      });
    owner.schedule(t+=240, function():Void {
      owner.say("Game", "I'll get to the artifacts before the robot does!");
      });
    owner.schedule(t+=180, function():Void {
      owner.say("Game", "You know, those crown-like things. Like that one down there.");
      h.action = FREE;
      });
    owner.schedule(t+=210, function():Void {
      owner.say("Game", "Speaking of the robot, there he is. Better get a move on.");
      });
  }

  override public function reset():Level1 {
    return new Level1(grid, map, owner);
  }
  override public function getNext():Level1 {
    return new Level1(grid, map, owner);
  }
}
