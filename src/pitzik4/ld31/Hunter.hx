package pitzik4.ld31;

import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.Sfx;
import pitzik4.ld31.Player.MageAction;

class Hunter extends PhysBasic {
  public static inline var ACCEL_X:Float = 0.1;
  public static inline var MAX_VEL_X:Float = 0.5;
  public static inline var MAX_VEL_Y:Float = 128.0;
  public static inline var FRICTION:Float = 0.05;
  public static inline var GRAVITY:Float = 0.25;

  public var sprite:Spritemap;
  public var flipped(get, set):Bool;
  public inline function get_flipped():Bool { return sprite.flipped; }
  public function set_flipped(val:Bool):Bool {
    if(val == sprite.flipped) return val;
    sprite.flipped = val;
    sprite.updateBuffer();
    return val;
  }
  public var action:MageAction = FREE;
  public var owner:MainScene;
  public var sfxGet:Sfx;

  public function new(owner:MainScene, x:Float = 0, y:Float = 0) {
    super(x, y);
    graphic = sprite = new Spritemap("graphics/hunter.png", 32, 32);
    this.owner = owner;

    sfxGet = new Sfx("audio/hunterget.wav");

    sprite.add("idle", [0], 1, false);
    sprite.add("run", [0, 1], 6, true);
    sprite.add("victory", [2, 3, 4], 10, true);

    maxVelocity.x = MAX_VEL_X;
    maxVelocity.y = MAX_VEL_Y;
    friction.x = FRICTION;
    acceleration.y = GRAVITY;

    solidType = ["o-solid", "d-solid"];
    type = "hunter";

    setHitbox(14, 31, -9, -1);
  }

  public function updateAI():Void {
    switch(action) {
      case FREE:
        var xp = (velocity.x+acceleration.x)*8;
        if(getCollides(xp) && velocity.y >= 0 && getCollides(0, 1)) {
          if(getCollides(xp, -32)) {
            flipped = !flipped;
          } else {
            jump();
          }
        }
        if(flipped) {
          acceleration.x = -ACCEL_X;
        } else {
          acceleration.x = ACCEL_X;
        }
        var art = collide("artifact", x, y);
        if(art != null && art.graphic.visible) {
          action = ANIMATE("victory");
          velocity.x = 0;
          sfxGet.play();
          owner.say("Game", [
            "Crud, crud, crud!",
            "Wait, did the robot just get the thing?",
            "Well, that didn't work.",
            "NOOOOOOOOOOOOO!",
            "DO NOT WANT!",
            "This stinks.",
            "FFFUUUUuuuu... I mean, CURSES!",
            "Let's take this from the top.",
            "BREADSTICKS!",
            "Oh, cinnamon!",
            "How about we pretend that didn't happen?",
            "Great, just great.",
            "Buh, buh, buh duhduh duhduhduh duh duddluh...",
            "G-u-i-l-t-y",
            "What a world! What a world!",
            "You can't get ye artifact.",
            "Oh, come on! If I can't do this, I might as well be in magic kindergarten!"
            ][Std.random(17)]);
          owner.schedule(120, owner.reset);
        }
      default: acceleration.x = 0;
    }
  }
  public function updateAnims():Void {
    switch(action) {
      case FREE:
        if(collideDown) {
          if(acceleration.x != 0) {
            sprite.play("run");
            if(acceleration.x > 0) {
              flipped = false;
            } else {
              flipped = true;
            }
          } else {
            sprite.play("idle");
          }
        } else {
          if(velocity.y < 0) {
            sprite.play("jump");
          } else {
            sprite.play("fall");
          }
        }
      case ANIMATE(anim):
        sprite.play(anim);
      default:
    }
  }
  public function jump():Void {
    velocity.y -= 5.0;
  }
  override public function update():Void {
    updateAI();
    updateAnims();
    super.update();
  }
}
