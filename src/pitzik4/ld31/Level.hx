package pitzik4.ld31;

import com.haxepunk.Entity;
import com.haxepunk.masks.Grid;
import com.haxepunk.graphics.Tilemap;
import com.haxepunk.Graphic;
import com.haxepunk.graphics.Stamp;
import openfl.Assets;

class Level extends Entity {
  public var grid:Grid;
  public var map:Tilemap;
  public var owner:MainScene;
  public var contents:Array<Entity>;
  public var graphics:Array<Graphic>;
  public var levelName:String;

  public function new(level:String, grid:Grid, map:Tilemap, owner:MainScene) {
    super();
    this.owner = owner;
    this.levelName = level;
    graphic = this.map = map; mask = this.grid = grid;
    type = "d-solid";
    contents = new Array<Entity>();
    graphics = new Array<Graphic>();
  }

  public function add(entity:Entity):Void {
    contents.push(entity);
    owner.addOther(entity);
  }
  public function remove(entity:Entity):Void {
    contents.remove(entity);
    owner.removeOther(entity);
  }
  public function addGfc(graphic:Graphic):Void {
    graphics.push(graphic);
    owner.addOtherGfc(graphic);
  }
  public function removeGfc(graphic:Graphic):Void {
    graphics.remove(graphic);
    owner.removeOtherGfc(graphic);
  }
  override public function added():Void {
    var level = Assets.getText("levels/"+levelName+".csv");
    grid.loadFromString(level);
    map.fill(new flash.geom.Rectangle(0,0,map.width,map.height), 0, 0);
    map.loadFromString(level);
  }
  override public function removed():Void {
    for(e in contents) {
      owner.removeOther(e);
    }
    for(g in graphics) {
      owner.removeOtherGfc(g);
    }
  }

  public function reset():Level {
    return new Level(levelName, grid, map, owner);
  }
  public function getNext():Level {
    return null;
  }

  public static function makeArtifact(x:Float, y:Float):Entity {
    var art = new Entity(x, y, new Stamp("graphics/artifact.png"));
    art.type = "artifact";
    art.setHitbox(16, 16);
    return art;
  }
}
