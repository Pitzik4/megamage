package pitzik4.ld31;

import openfl.Assets;
import com.haxepunk.Scene;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Tilemap;
import com.haxepunk.graphics.Stamp;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.Sfx;
import com.haxepunk.graphics.Graphiclist;
import com.haxepunk.HXP;
import com.haxepunk.masks.Grid;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Backdrop;
import com.haxepunk.Graphic;
import openfl.display.BitmapData;
import openfl.display.BitmapDataChannel;
import openfl.display.Shape;
import openfl.display.BlendMode;
import openfl.utils.ByteArray;
import pitzik4.ld31.level.*;

@:access(com.haxepunk.Graphic)
class MainScene extends Scene {
  public static inline var WIDTH:Int = 20;
  public static inline var HEIGHT:Int = 15;
  public static inline var TWIDTH:Int = 16;
  public static inline var THEIGHT:Int = 16;
  public static inline var START_X:Float = 3*16;
  public static inline var START_Y:Float = 3*16;

  public var sg:ShadowGrid;
  public var sshape:Shape;
  public var shadows:BitmapData;
  public var player:Player;
  public var shadowTime:Int = 0;
  public var bgm:Sfx;
  public var world2Gfx:Graphiclist;
  public var world2Bitmap:BitmapData;
  public var home:Entity;
  public var level:Level;
  public var levelGrid:Grid;
  public var levelTilemap:Tilemap;
  public var marker:Marker;
  public var spliceGrid:Grid;
  public var spliceEnt:Entity;
  public var actors:Map<String, Entity>;
  public var fader:Image;
  public var fadeSpeed:Float = 0;
  public var fadingOut:Bool = false;
  public var fadeOther:Bool = false;
  public var sfxMarker:Sfx;
  public var sfxReset:Sfx;
  public var artifact:Entity;

  private var _jobTimes:Array<Int>;
  private var _jobs:Array<Void->Void>;
  private var _jobsLevelly:Array<Bool>;

  override public function begin():Void {
    #if flash
    bgm = new Sfx("audio/megamagic.mp3");
    #else
    bgm = new Sfx("audio/megamagic.ogg");
    #end
    bgm.loop();
    _jobTimes = new Array<Int>();
    _jobs = new Array<Void->Void>();
    _jobsLevelly = new Array<Bool>();
    actors = new Map<String, Entity>();
    sfxMarker = new Sfx("audio/marker.wav");
    sfxReset = new Sfx("audio/reset.wav");
    fader = new Image(HXP.createBitmap(Main.WIDTH, Main.HEIGHT, false, 0xFFFFFFFF));
    fader.alpha = 0;
    fader.color = 0xFF000000;
    addGraphic(new Backdrop("graphics/bg.png"), 32);
    var homestar = Assets.getText("levels/home.csv");
    home = new Entity();
    home.layer = 16;
    home.mask = sg = new ShadowGrid(WIDTH*TWIDTH, HEIGHT*THEIGHT, TWIDTH, THEIGHT);
    sg.loadFromString(homestar);
    home.type = "solid";
    var tm = new Tilemap("graphics/tiles.png", WIDTH*TWIDTH, HEIGHT*THEIGHT, TWIDTH, THEIGHT);
    home.graphic = tm;
    tm.loadFromString(homestar);
    add(home);
    addGraphic(new Stamp("graphics/door.png"), 16, START_X, START_Y);
    addGraphic(new Stamp("graphics/bars.png"), 16, 14*16, 2*16);
    addGraphic(new Stamp("graphics/bars.png"), 16, 5*16, 6*16);
    addGraphic(new Stamp("graphics/bars.png"), 16, 2*16, 10*16);
    addGraphic(new Stamp("graphics/bars.png"), 16, 10*16, 10*16);
    world2Gfx = new Graphiclist();
    shadows = new BitmapData(WIDTH*TWIDTH, HEIGHT*THEIGHT, true, 0);
    addGraphic(new Stamp(world2Bitmap = new BitmapData(WIDTH*TWIDTH, HEIGHT*THEIGHT, true, 0)), 8);
    sshape = new Shape();
    add(player = new Player(this, START_X, START_Y));
    addActor("Game", player);
    artifact = addGraphic(new Stamp("graphics/artifact-off.png"), 12, -16, -16);
    levelTilemap = new Tilemap("graphics/tiles.png", WIDTH*TWIDTH, HEIGHT*THEIGHT, TWIDTH, THEIGHT);
    levelGrid = new Grid(WIDTH*TWIDTH, HEIGHT*THEIGHT, TWIDTH, THEIGHT);
    add(marker = new Marker());
    loadLevel(new LevelHome(levelGrid, levelTilemap, this), false);
    spliceGrid = new Grid(WIDTH*TWIDTH, HEIGHT*THEIGHT, TWIDTH, THEIGHT);
    add(spliceEnt = new Entity(0, 0, null, spliceGrid));
    spliceEnt.type = "null";
  }

  public function loadLevel(newLevel:Level, effects:Bool = true, seamless:Bool = false):Void {
    if(effects) {
      fade(1/3);
      sfxReset.play();
    }
    if(!seamless) {
      player.x = START_X; player.y = START_Y;
      player.velocity.x = player.velocity.y = player.acceleration.x = 0;
      player.flipped = false;
      player.action = FREE; player.sfxLaugh.stop();
      if(marker.visible) removeMarker(false);
      artifact.visible = true;
      var i = 0;
      while(i < _jobs.length) {
        if(_jobsLevelly[i]) {
          _jobs.splice(i, 1);
          _jobTimes.splice(i, 1);
          _jobsLevelly.splice(i, 1);
        } else {
          ++i;
        }
      }
      var dialogues = entitiesForType("dialogue");
      if(dialogues != null) for(d in dialogues) remove(d);
    }
    if(level != null)
      removeOther(level);
    level = newLevel;
    addOther(level);
  }

  public function addOtherGfc(gfc:Graphic):Void {
    if(gfc != null)
      world2Gfx.add(gfc);
  }
  public function removeOtherGfc(gfc:Graphic):Void {
    world2Gfx.remove(gfc);
  }
  public function addOther(ent:Entity):Void {
    ent.visible = false;
    ent.graphic._entity = ent;
    addOtherGfc(ent.graphic);
    add(ent);
  }
  public function removeOther(ent:Entity):Void {
    removeOtherGfc(ent.graphic);
    remove(ent);
  }

  public function reset():Void {
    sfxReset.play();
    loadLevel(level.reset());
  }

  public function beginLevelSwitch(newLevel:Level, seamless:Bool = false):Void {
    fade(1, 0xFF000000, 0, true, true);
    schedule(60, loadLevel.bind(newLevel, true, seamless));
  }

  public function addActor(name:String, actor:Entity):Void {
    actors.set(name, actor);
  }
  public function removeActor(name:String):Bool {
    return actors.remove(name);
  }

  override public function update():Void {
    if(Input.pressed("reset")) reset();
    super.update();
    if(Input.pressed("skip")) beginLevelSwitch(level.getNext());
    if(!marker.visible)
      shadowTime++;
    if(shadowTime > 1) {
      updateShadows();
      shadowTime = 0;
    }
    var i = 0;
    while(i < _jobs.length) {
      _jobTimes[i]--;
      if(_jobTimes[i] < 0) {
        _jobs[i]();
        _jobs.splice(i, 1); _jobTimes.splice(i, 1); _jobsLevelly.splice(i, 1);
      } else {
        i++;
      }
    }
    if(fadingOut)
      fader.alpha = Math.min(fader.alpha+fadeSpeed, 1);
    else
      fader.alpha = Math.max(fader.alpha-fadeSpeed, 0);
  }
  override public function render():Void {
    for(g in world2Gfx.children) {
      if(g.relative && g._entity != null) {
        g.x = g._entity.x; g.y = g._entity.y;
      }
    }
    world2Bitmap.fillRect(world2Bitmap.rect, 0xFF7D9EFF);
    world2Gfx.render(world2Bitmap, HXP.zero, HXP.zero);
    if(fadeOther) fader.render(world2Bitmap, HXP.zero, HXP.zero);
    world2Bitmap.copyChannel(shadows, shadows.rect, HXP.zero, BitmapDataChannel.ALPHA, BitmapDataChannel.ALPHA);
    super.render();
    if(!fadeOther) fader.render(HXP.buffer, HXP.zero, HXP.zero);
  }
  public function updateShadows():Void {
    var points = marker.visible?sg.shadows(marker.x, marker.y):sg.shadows(player.centerX, player.centerY);
    sshape.graphics.clear();
    //sshape.graphics.beginFill(0, marker.visible?1:0.5);
    sshape.graphics.beginFill(0, 1);
    sshape.graphics.moveTo(points[0].x, points[0].y);
    var i:Int = 1;
    while(i < points.length) {
      sshape.graphics.lineTo(points[i].x, points[i].y);
      i++;
    }
    sshape.graphics.endFill();
    shadows.fillRect(shadows.rect, 0);
    shadows.draw(sshape, null, null, null, null, false);
  }
  public function schedule(time:Int, job:Void->Void, levelly:Bool = true):Void {
    _jobTimes.push(time); _jobs.push(job); _jobsLevelly.push(levelly);
  }
  public static function spliceGrids(on:Grid, off:Grid, ref:BitmapData, into:Grid):Void {
    var width = into.columns, height = into.rows;
    var tw = into.tileWidth, th = into.tileHeight;
    for(y in 0...height) {
      for(x in 0...width) {
        var count = 0;
        for(iy in y*th...y*th+th) {
          for(ix in x*tw...x*tw+tw) {
            #if flash
            if(Std.int(ref.getPixel32(ix, iy)) < 0) count++;
            #else
            if(ref.getPixel32(ix, iy) < 0) count++;
            #end
          }
        }
        into.setTile(x, y, count > tw*th*0.5 ? on.getTile(x, y) : off.getTile(x, y));
      }
    }
  }
  public function setMarker(x:Float, y:Float):Void {
    marker.goTo(x, y);
    home.type = "null";
    level.type = "o-solid";
    updateShadows();
    spliceGrids(levelGrid, sg, shadows, spliceGrid);
    spliceEnt.type = "solid";
    fade(1/3);
    sfxMarker.play(0.5);
  }
  public function removeMarker(fanfare:Bool = true):Void {
    marker.disable();
    home.type = "solid";
    level.type = "d-solid";
    spliceEnt.type = "null";
    if(fanfare) {
      fade(1/3);
      sfxMarker.play(0.5);
    }
  }
  public function raiseDialogue(dialogue:Dialogue, amt:Float = 12, ?ignore:Array<Dialogue>):Void {
    var collisions = new Array<Dialogue>();
    dialogue.collideInto("dialogue", dialogue.x, dialogue.y-amt, collisions);
    dialogue.raise(amt);
    var ignorer:Array<Dialogue> = ignore==null?collisions:ignore.concat(collisions);
    for(c in collisions) if(ignore==null||ignore.indexOf(c)<0) raiseDialogue(c, amt, ignorer);
  }
  public function say(actor:String, text:String):Void {
    var a = actors.get(actor);
    var d = new Dialogue(a.centerX, a.y, text, this, a);
    add(d);
    updateLists();
    #if flash
    raiseDialogue(d, d.height-4);
    #else
    raiseDialogue(d, d.height);
    #end
  }
  public function fade(time:Float, color:Int = 0xFFFFFFFF, alpha:Float = 1, out:Bool = false, other:Bool = false):Void {
    fader.color = color;
    fadeSpeed = (1/60) / time;
    fader.alpha = alpha;
    fadingOut = out;
    fadeOther = other;
  }
}
