package pitzik4.ld31;

import com.haxepunk.masks.Grid;
import flash.geom.Point;

class ShadowGrid extends Grid {
  public var corners:Array<Point>;
  private var _points:Array<Point>;
  private var _point3:Point;
  private var _cornersDirty:Bool = false;

  public function new(width:Int, height:Int, tileWidth:Int, tileHeight:Int, x:Int = 0, y:Int = 0) {
    super(width, height, tileWidth, tileHeight, x, y);
    corners = [new Point(0,0),new Point(width,0),new Point(0,height),new Point(width,height)];
    _points = new Array<Point>();
    _point3 = new Point();
  }

  override private function setTileXY(x:Int = 0, y:Int = 0, solid:Bool = true):Void {
    if(!checkTile(x, y) || data[y][x] == solid) return;
    data[y][x] = solid; _cornersDirty = true;
  }

  public function calculateCorners():Void {
    while(corners.length > 0) _points.push(corners.pop());
    for(y in 0...rows+1) {
      for(x in 0...columns+1) {
        var ul=getTileAt(x-1,y-1),ur=getTileAt(x,y-1),bl=getTileAt(x-1,y),br=getTileAt(x,y);
        if((ul==br||ur==bl) && (ul!=ur||ur!=br||br!=bl||bl!=ul)) {
          corners.push(recyclePoint(x*tileWidth, y*tileHeight));
        }
      }
    }
  }

  public inline function getTileAt(x:Int, y:Int):Bool {
    if(x < 0 || x >= columns || y < 0 || y >= rows) return true;
    else return data[y][x];
  }
  public inline function getTileAtPixel(x:Float, y:Float):Bool {
    if(x < 0 || x >= columns*tileWidth || y < 0 || y >= rows*tileHeight) return true;
    else return data[Std.int(y/tileHeight)][Std.int(x/tileWidth)];
  }

  private function recyclePoint(x:Float = 0, y:Float = 0):Point {
    var out = _points.pop();
    if(out == null) return new Point(x, y);
    out.x = x; out.y = y;
    return out;
  }

  public function raycast(x:Float, y:Float, dx:Float, dy:Float, ?collisionPoint:Point):Point {
    if(collisionPoint == null) collisionPoint = _point3;
    x /= tileWidth; y /= tileHeight;
    var rx = dx/dy, ry = dy/dx;
    var sx = dx<0?-1:1, sy = dy<0?-1:1;
    while(!getTileAt(dx<0?Math.ceil(x)-1:Math.floor(x), dy<0?Math.ceil(y)-1:Math.floor(y))) {
      var xd = Math.floor(x)-x; if(dx>=0) xd = 1+xd; else if(xd==0) xd = -1;
      var yd = Math.floor(y)-y; if(dy>=0) yd = 1+yd; else if(yd==0) yd = -1;
      if(xd*sx > Math.abs(yd*rx)) {
        y += yd; x += yd*rx;
      } else {
        x += xd; y += xd*ry;
      }
    }
    collisionPoint.x = x*tileWidth; collisionPoint.y = y*tileHeight;
    return collisionPoint;
  }
  public inline function distSquared(dx:Float, dy:Float):Float {
    return dx*dx+dy*dy;
  }
  public function shadows(x:Float, y:Float, recycle:Bool = true):Array<Point> {
    if(_cornersDirty) calculateCorners();
    var hits = new Array<Point>();
    for(c in corners) {
      var dx = c.x-x, dy = c.y-y;
      for(m in [-0.01,0.01])
        hits.push(raycast(x, y, dx-dy*m, dy+dx*m, recyclePoint()));
      var rc = raycast(x, y, dx, dy, recyclePoint());
      if(distSquared(rc.x-x, rc.y-y) > distSquared(c.x-x, c.y-y)) {
        rc.x = c.x; rc.y = c.y;
      }
      hits.push(rc);
    }
    if(recycle) for(h in hits) _points.push(h);
    _point3.x = x; _point3.y = y;
    hits.sort(angleComp.bind(_point3));
    return hits;
  }

  public static function angleComp(c:Point, a:Point, b:Point):Int {
    if (a.x - c.x >= 0 && b.x - c.x < 0)
      return 1;
    if (a.x - c.x < 0 && b.x - c.x >= 0)
      return -1;
    if (a.x - c.x == 0 && b.x - c.x == 0) {
      if (a.y - c.y >= 0 || b.y - c.y >= 0)
        return a.y > b.y ? 1 : -1;
      return b.y > a.y ? 1 : -1;
    }

    // compute the cross product of vectors (c -> a) x (c -> b)
    var det:Float = (a.x - c.x) * (b.y - c.y) - (b.x - c.x) * (a.y - c.y);
    if (det < 0)
      return 1;
    if (det > 0)
      return -1;

    // points a and b are on the same line from the c
    // check which point is closer to the c
    var d1:Float = (a.x - c.x) * (a.x - c.x) + (a.y - c.y) * (a.y - c.y);
    var d2:Float = (b.x - c.x) * (b.x - c.x) + (b.y - c.y) * (b.y - c.y);
    return d1 > d2 ? 1 : -1;
  }
}
