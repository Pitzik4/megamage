package pitzik4.ld31;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Text;
import flash.text.TextFormatAlign;

class Dialogue extends Entity {
  public var text:Text;
  public var bump:Float = 0;
  public var lifetime:Int = 240;
  public var owner:MainScene;
  public var attached:Entity;

  private var _ax:Float;
  private var _ay:Float;

  public function new(x:Float, y:Float, text:String, ?owner:MainScene, ?attached:Entity) {
    super(x, y, this.text = new Text(text, -64, 0, 128, 0, { color: 0xFFFFFF10, size: 8, wordWrap: true }));
    this.text.alpha = 0;
    setHitbox(this.text.width, this.text.height, -Std.int(this.text.x), -Std.int(this.text.y));
    type = "dialogue";
    this.owner = owner;
    this.attached = attached;
    if(attached != null) {
      _ax = attached.x; _ay = attached.y;
    }
    layer = -8;
  }

  override public function update():Void {
    y -= bump*0.05;
    bump *= 0.95;
    lifetime--;
    text.alpha = Math.min(4 - lifetime/60, lifetime/60);
    if(lifetime < 0) {
      owner.remove(this);
    }
    if(attached != null) {
      x += attached.x - _ax; y += attached.y - _ay;
      _ax = attached.x; _ay = attached.y;
    }
    super.update();
  }
  public function raise(amount:Float = 8):Void {
    bump += amount;
  }
}
