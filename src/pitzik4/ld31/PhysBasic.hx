package pitzik4.ld31;

import flash.geom.Point;
import com.haxepunk.Entity;
import com.haxepunk.HXP;

class PhysBasic extends Entity {
  public var maxVelocity:Point;
  public var velocity:Point;
  public var acceleration:Point;
  public var friction:Point;
  public var solidType:SolidType;

  private var motionX:Float;
  private var motionY:Float;

  public var collideUp(get, never):Bool;
  public inline function get_collideUp():Bool {
    return getCollides(0,-1);
  }
  public var collideRight(get, never):Bool;
  public inline function get_collideRight():Bool {
    return getCollides(1,0);
  }
  public var collideDown(get, never):Bool;
  public inline function get_collideDown():Bool {
    return getCollides(0,1);
  }
  public var collideLeft(get, never):Bool;
  public inline function get_collideLeft():Bool {
    return getCollides(-1,0);
  }

  public static inline function addPoints(p1:Point, p2:Point) {
    p1.x += p2.x; p1.y += p2.y;
  }
  public static inline function clampVelocity(p1:Point, p2:Point) {
    p1.x = HXP.clamp(p1.x, -p2.x, p2.x);
    p1.y = HXP.clamp(p1.y, -p2.y, p2.y);
  }
  public static inline function approachZero(v:Point, f:Point) {
    v.x = HXP.approach(v.x, 0, f.x);
    v.y = HXP.approach(v.y, 0, f.y);
  }

  public function new(?x:Float, ?y:Float) {
    super(x, y);
    maxVelocity = new Point();
    velocity = new Point();
    acceleration = new Point();
    friction = new Point();
  }

  override public function moveCollideX(e:Entity):Bool {
    velocity.x = 0;
    return true;
  }
  override public function moveCollideY(e:Entity):Bool {
    velocity.y = 0;
    return true;
  }

  public function updatePhysics() {
    addPoints(velocity, acceleration);
    approachZero(velocity, friction);
    clampVelocity(velocity, maxVelocity);
    moveBy(velocity.x, velocity.y, solidType);
  }
  override public function update() {
    updatePhysics();
    super.update();
  }
  public function getCollides(offsX:Float=0, offsY:Float=0):Bool {
    var arr:Array<String> = null;
    switch(solidType.type) {
      case Left(str): arr = [str];
      case Right(array): arr = array;
    }
    for(t in arr) {
      if(collide(t, x+offsX, y+offsY) != null) {
        return true;
      }
    }
    return false;
  }

  public function entUpdate() {
    super.update();
  }

  override public function moveBy(x:Float, y:Float, solidType:SolidType = null, sweep:Bool = false) {
    motionX = x; motionY = y;
    super.moveBy(x, y, solidType, sweep);
  }
}
