package pitzik4.ld31;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;

class Marker extends Entity {
  public var sprite:Spritemap;

  public function new(x:Float = 0, y:Float = 0) {
    super(x, y, sprite = new Spritemap("graphics/marker.png", 16, 16));
    sprite.add("twinkle", [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,5], 10, true);
    sprite.play("twinkle");
    sprite.originX = sprite.originY = 8;
    visible = false;
    layer = -16;
  }

  public function goTo(x:Float, y:Float):Void {
    visible = true; this.x = x; this.y = y;
  }
  public function disable():Void {
    visible = false;
  }
}