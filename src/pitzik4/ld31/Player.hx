package pitzik4.ld31;

import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.Sfx;

enum MageAction {
  FREE;
  ANIMATE(anim:String);
  CUTSCENE;
}
@:access(com.haxepunk.graphics.Spritemap)
class Player extends PhysBasic {
  public static inline var ACCEL_X:Float = 0.25;
  public static inline var MAX_VEL_X:Float = 2.0;
  public static inline var MAX_VEL_Y:Float = 128.0;
  public static inline var FRICTION:Float = 0.125;
  public static inline var GRAVITY:Float = 0.25;

  public var sprite:Spritemap;
  public var flipped(get, set):Bool;
  public inline function get_flipped():Bool { return sprite.flipped; }
  public function set_flipped(val:Bool):Bool {
    if(val == sprite.flipped) return val;
    sprite.flipped = val;
    sprite.updateBuffer();
    return val;
  }
  public var action:MageAction = FREE;
  public var sfxLaugh:Sfx;
  public var sfxFail:Sfx;
  public var owner:MainScene;

  public function new(owner:MainScene, x:Float = 0, y:Float = 0) {
    super(x, y);
    graphic = sprite = new Spritemap("graphics/mega.png", 32, 32);
    this.owner = owner;

    sprite.add("laugh", [1, 2, 3, 4, 4, 4, 4, 5, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 5, 4, 4, 4, 4, 3, 2, 1, 0], 10, false);
    sprite.add("cast", [1, 2, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 3, 2, 1, 0], 10, false);
    sprite.add("idle", [0], 1, false);
    sprite.add("run", [6,7,8,9,10,11], 10, true);
    sprite.add("jump", [12], 1, false);
    sprite.add("fall", [13,14], 10, false);
    sprite.add("victory", [15,16], 6, true);

    defineInputs();

    maxVelocity.x = MAX_VEL_X;
    maxVelocity.y = MAX_VEL_Y;
    friction.x = FRICTION;
    acceleration.y = GRAVITY;

    solidType = ["solid", "d-solid"];
    type = "player";

    sfxLaugh = new Sfx("audio/laugh.wav");
    sfxFail = new Sfx("audio/fail.wav");

    setHitbox(14, 31, -9, -1);
  }

  public static function defineInputs():Void {
    Input.define("right", [Key.RIGHT, Key.D]);
    Input.define("left", [Key.LEFT, Key.A]);
    Input.define("jump", [Key.UP, Key.Z, Key.W]);
    Input.define("magic", [Key.X, Key.SPACE]);
    Input.define("reset", [Key.Q, Key.ENTER]);
    Input.define("skip", [Key.P]);
  }

  public function updateInput():Void {
    switch(action) {
      case FREE:
        acceleration.x = 0;
        if(Input.check("right")) {
          acceleration.x += ACCEL_X;
        }
        if(Input.check("left")) {
          acceleration.x -= ACCEL_X;
        }
        if(Input.pressed("jump") && collideDown) {
          jump();
        }
        if(Input.pressed("magic")) {
          if(owner.marker.visible) {
            if(collide("o-solid", x, y) == null)
              owner.removeMarker();
            else
              sfxFail.play(0.5);
          } else {
            action = ANIMATE("laugh");
            sfxLaugh.play();
            acceleration.x = velocity.x = 0;
            var xp = centerX, yp = centerY;
            owner.schedule(160, function():Void {
              owner.setMarker(xp, yp);
              });
          }
        }
        var art = collide("artifact", x, y);
        if(art != null) {
          art.graphic.visible = false;
          owner.artifact.visible = false;
          action = ANIMATE("victory");
          sfxLaugh.play();
          owner.schedule(120, owner.beginLevelSwitch.bind(owner.level.getNext(), false));
        }
      case ANIMATE(anim):
        if(!sprite._anim.loop && sprite.index >= sprite._anim.frameCount-1) action = FREE;
      default:
    }
  }
  public function updateAnims():Void {
    switch(action) {
      case FREE:
        if(collideDown) {
          if(acceleration.x != 0) {
            sprite.play("run");
            if(acceleration.x > 0) {
              flipped = false;
            } else {
              flipped = true;
            }
          } else {
            sprite.play("idle");
          }
        } else {
          if(velocity.y < 0) {
            sprite.play("jump");
          } else {
            sprite.play("fall");
          }
        }
      case ANIMATE(anim): sprite.play(anim);
      default:
    }
  }
  public function jump():Void {
    velocity.y -= 5.5;
  }
  override public function update():Void {
    updateInput();
    updateAnims();
    super.update();
  }
}
