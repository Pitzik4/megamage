package pitzik4.ld31;

import com.haxepunk.Engine;
import com.haxepunk.HXP;
import com.haxepunk.RenderMode;

class Main extends Engine {
  public static inline var WIDTH:Int = 320;
  public static inline var HEIGHT:Int = 240;

  public function new() {
    super(WIDTH, HEIGHT, 60, false, RenderMode.BUFFER);
  }

  override public function init() {
#if debug
    HXP.console.enable();
#end
    HXP.scene = new MainScene();
  }

  public static function main() { new Main(); }
}
