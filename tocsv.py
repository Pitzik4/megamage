#!/usr/bin/env python

import sys

name = 'assets/levels/'+sys.argv[1]
with open(name+'.json', 'r') as content_file:
  content = content_file.read()
pos = content.rindex('data')+7
data = content[pos:content.index(']', pos)].split(', ')
data = [str(max(int(x)-1,0)) for x in data]
pos = content.index('width')+7
width = int(content[pos:min(content.index(',', pos), content.index('\n', pos))])
height = len(data)//width
with open(name+'.csv', 'w') as out_file:
  for i in range(0, height):
    for j in range(0, width):
      out_file.write(data[i*width+j])
      if j < width-1:
        out_file.write(',')
    if i < height-1:
      out_file.write('\n')
